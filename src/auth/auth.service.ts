// import { Injectable } from '@nestjs/common';
// import { UsersService } from "../users/users.service"
// import { JwtService } from "@nestjs/jwt"
// import { jwtConstant } from './constant'

// @Injectable()
// export class AuthService {
//     constructor(
//         private userService : UsersService,
//         private jwtService : JwtService
//     ){}

//     async validateUser (username : string, password : string) : Promise<any> {
        
//         const user = await this.userService.findOne(username)

//         if (user && user.password == password) {
//             const { ...result } = user
//             return result
//         }
//         return null
//     }

//     async login(user : any) : Promise<any> {
//         const payload = { username: user.username, password: user.password, sub: user.userId }
//         return {
//             access_token: await this.jwtService.sign(payload),
//             expireIn: this.getTimeToExpireToken()
//         }
//     }

//     getTimeToExpireToken() : string {
//         return this.convertSecondToMilisecondToken(parseInt(jwtConstant.expireIn.substr(0, 2)))
//     }

//     convertSecondToMilisecondToken(seconds:number ) : string {
//         const miliseconds : number = seconds * 1000
//         return new Date(Date.now() + miliseconds).toISOString()
//     }
// }
