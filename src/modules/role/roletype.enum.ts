export enum RoleType {
    ADMIN = 'ADMIN',
    CLIENT = 'CLIENT',
    USER = 'USER'
}