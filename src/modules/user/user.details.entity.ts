import { 
    BaseEntity, 
    Entity, 
    // PrimaryColumn, 
    PrimaryGeneratedColumn, 
    Column, 
    UpdateDateColumn,
    CreateDateColumn
} from 'typeorm'

@Entity('users_details')
export class UserDetails extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ type : 'varchar' , length : 50, nullable : true })
    name: string;

    @Column({ type : 'varchar' , length : 50, nullable: true })
    lastname: string;

    @Column({ type : 'varchar', default : 'ACTIVE', length : 8 })
    status:string;

    @CreateDateColumn({ type : 'timestamp', name : 'createAt', nullable : true })
    createAt:Date;

    @UpdateDateColumn({ type : 'timestamp', name : 'updateAt', nullable : true})
    updateAt:Date;
}