import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';
import { SharedModule } from 'src/shared/shared.module';
import { UserController } from './user.controller';
import { RoleRepository } from './../role/role.repository'

@Module({
    imports:[
        TypeOrmModule.forFeature([UserRepository, RoleRepository]),
        SharedModule
    ],
    providers: [UserService],
    controllers: [UserController]
})
export class UserModule {}



