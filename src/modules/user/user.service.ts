import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { MapperService } from 'src/shared/mapper.service';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { UserDetails } from './user.details.entity';
import { RoleRepository } from '../role/role.repository';

@Injectable()
export class UserService {
    constructor (
        @InjectRepository(UserRepository)
        private readonly _userRepository : UserRepository,
        @InjectRepository(RoleRepository)
        private readonly _roleRepository : RoleRepository,
        private readonly _mapperService : MapperService
    ) {}

    async get(id: number) : Promise <UserDto> {
        if (!id) {
            throw new BadRequestException('id must be sent')
        }
        const user : User = await this._userRepository.findOne(id, {
            where: { status : 'ACTIVE'}
        })

        if (!user) {
            throw new NotFoundException()
        }
        return this._mapperService.map<User, UserDto>(user, new UserDto())
    }

    async getAll() : Promise <UserDto[]> {
        
        const users : User[] = await this._userRepository.find( {
            where: { status : 'ACTIVE'}
        })

        if (!users) {
            throw new NotFoundException()
        }
        return this._mapperService.mapCollection<User, UserDto>(users, new UserDto())
    }

    async create(user: User) : Promise<UserDto>{
        const details = new UserDetails()
        user.details = details
        const defaultRole = await this._roleRepository.findOne({where: { name: 'GENERAL'}})
        user.roles = [defaultRole] 

        const savedUser : User = await this._userRepository.save(user)
        return this._mapperService.map<User, UserDto>(savedUser, new User())
    }

    async update(id: number, user: User) : Promise<void> {
        await this._userRepository.update(id, user)
    } 

    async delete(id: number) : Promise<void> {
        const userExist = await this._userRepository.findOne(id, {
            where: { status : 'ACTIVE' }
        })
        
        if(!userExist){
            throw new NotFoundException()
        }
        
        await this._userRepository.update(id, {
            status: 'INACTIVE'
        }) 
        
    }
}
