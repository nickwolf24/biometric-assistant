import {MigrationInterface, QueryRunner} from "typeorm";

export class fixNameDetail1596136101102 implements MigrationInterface {
    name = 'fixNameDetail1596136101102'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users_details" RENAME COLUMN "username" TO "name"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users_details" RENAME COLUMN "name" TO "username"`);
    }

}
