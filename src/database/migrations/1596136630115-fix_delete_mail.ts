import {MigrationInterface, QueryRunner} from "typeorm";

export class fixDeleteMail1596136630115 implements MigrationInterface {
    name = 'fixDeleteMail1596136630115'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users_details" DROP COLUMN "email"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users_details" ADD "email" character varying NOT NULL`);
    }

}
